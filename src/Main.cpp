/* Copyright (C) 2013 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 * and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable. */

#ifdef _WIN32
# include <Windows.h>
# define sleep(seconds) Sleep(seconds * 1000);
#else
# include <unistd.h>
# include <stdio.h>
#endif

#include "PosixTestClient.h"

const unsigned MAX_ATTEMPTS = 50;
const unsigned SLEEP_TIME = 10;
const unsigned PORT = 7496;

int main(int argc, char** argv) {
  const char* host = argc > 1 ? argv[1] : "";
  int clientId = 0;

  unsigned attempt = 0;
  printf("Start of POSIX Socket Client Test %u\n", attempt);

  while (true) {
    ++attempt;
    printf("Attempt %u of %u\n", attempt, MAX_ATTEMPTS);

    PosixTestClient client;

    client.connect(host, PORT, clientId);

    while (client.isConnected()) {
      client.processMessages();
    }

    if (attempt >= MAX_ATTEMPTS) {
      break;
    }

    printf("Sleeping %u seconds before next attempt\n", SLEEP_TIME);
    sleep(SLEEP_TIME);
  }

  printf("End of POSIX Socket Client Test\n");
}
