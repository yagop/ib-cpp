TARGET=PosixSocketClientTest

IB_SRC_DIR=./libs/IBPosixClient/src
IB_SHARED_DIR=./libs/IBPosixClient/Shared
POSIX_CLIENT_DIR=./libs/PosixTestClient
OUTPUT=./out

CXX=clang++
CXXFLAGS=-DIB_USE_STD_STRING -Wall -Wno-switch
INCLUDES=-I${IB_SRC_DIR} -I${IB_SHARED_DIR} -I${POSIX_CLIENT_DIR}

$(TARGET):
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o ${OUTPUT}/EClientSocketBase.o -c $(IB_SRC_DIR)/EClientSocketBase.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o ${OUTPUT}/EPosixClientSocket.o -c $(IB_SRC_DIR)/EPosixClientSocket.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o ${OUTPUT}/PosixTestClient.o -c ${POSIX_CLIENT_DIR}/PosixTestClient.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o ${OUTPUT}/Main.o -c ./src/Main.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o ${OUTPUT}/$@ ${OUTPUT}/EClientSocketBase.o ${OUTPUT}/EPosixClientSocket.o ${OUTPUT}/PosixTestClient.o ${OUTPUT}/Main.o

clean:
	rm -f out/*
